import sys
import re

operand = ['+','-','*','/','<<','>>']

f = open('input.txt','r')
for line in f.readlines():
    l=line.split()
    print l
    __length__=len(l)
    if __length__ == 4:
        if l[0] in operand:
            print "valid operator"
        else:
            print "invalid operator"

        match=re.match("^[_]*[-]?[a-zA-Z][a-zA-Z0-9]*[_]*$",l[1]) or re.match("^[0-9]*$",l[1])
        if match:
            print "valid operand 1"
        else:
            print "invalid operand 1"

        match=re.match("^[_]*[-]?[a-zA-Z_][a-zA-Z0-9]*[_]*$",l[2]) or re.match("^[0-9]*$",l[2])
        if match:
            print "valid operand 2"
        else:
            print "invalid operand 2"

        match=re.match("^[_]*[a-zA-Z_][a-zA-Z0-9_]*$",l[3])
        if match:
            print "valid operand 3"
        else:
            print "invalid operand 3"
    else:
        print "invalid number of operands"
