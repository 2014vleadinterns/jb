from notes import *

def run_note_tests():
    def test_reset_notes():
    	reset_notes()
    	assert(get_notes()==[])
    
    def test_addnote():
    	new_note("bhaskar", "Good Morning")  
    	new_note("jb", "afternoon")
    	new_note("jaya", "Evening")
    	new_note("reddy", "night")
    	assert(get_notes()==[("bhaskar", "Good Morning"),("jb", "afternoon"),("jaya", "Evening"),("reddy", "night")])
        print "Initial Notes: ", get_notes()
    
    def test_findnote():
        find = find_notes("reddy")
  	print "Notes finded: ", find
    
    def test_delete_note_by_title():
    	delete_note_by_title("tt")
        print "Notes after Deletion: ", get_notes()

    def test_delete_note_by_bodykey():
        delete_note_by_bodykey("Night")
        print "Notes after Deletion: ", get_notes()
    def test_save_note():
        save_note()
        print "notes saved"

    test_reset_notes()
    test_addnote()
    test_findnote()
    test_delete_note_by_title()
    test_delete_note_by_bodykey()
    test_save_note()

run_note_tests()         
