import re
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET


__notes__ = []
__find__  = []



def new_note(title,body):
    global __notes__
    if title == None:
	title = "Untitled Note"
    note = (title,body)
    __notes__.append(note)    

def find_notes(key):
    global __notes__
    find = []
    if key == None:
	key = "Untitled Key"
    for x in __notes__:
        if re.search(key, x[0], re.IGNORECASE) or re.search(key, x[1], re.IGNORECASE):
            find.append([x])
    return find
	    
def delete_note_by_title(title):
    global __notes__
    if title==None:
        title="Untitled"
    for x in __notes__:
        if re.search(title, x[0], re.IGNORECASE):
            delete = [x]
            __notes__.remove(delete[0])

def delete_note_by_bodykey(key):
    global __notes__
    if key==None:
        key="Untitled"
    for x in __notes__:
        if re.search(key, x[0], re.IGNORECASE) or re.search(key, x[1], re.IGNORECASE):
            delete = [x]
            __notes__.remove(delete[0])

def save_note():  
    global __notes__
    root = ET.Element("Master_Note")
    for x in __notes__:
        doc = ET.SubElement(root, "Note")
        field1 = ET.SubElement(doc, "Title")
        field1.text = x[0]
        field2 = ET.SubElement(doc, "Content")
        field2.text = x[1]
    tree = ET.ElementTree(root)
    tree.write("Notes.xml")


def get_notes():
    global __notes__
    return __notes__

def reset_notes():
    global __notes__
    __notes__=[] 
